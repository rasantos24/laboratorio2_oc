module regi_File(
	input [2:0] readAddr1,
	input [2:0] readAddr2,
	input [2:0] writeAddr,
	input [2:0] op,
	input beqz,
	input bnez,
	input [7:0] dir_branch
	input [63:0] writeData,
	input clk,
	input w_enable,
	output [31:0] readData1,
	output [31:0] readData2
	);

reg [22:0] rom[0:255]

always @(posedge clk)
	begin
		if(w_enable == 1'b1)
			rom[writeAddr] = writeData,
	end

assign readData1 = rom[readAddr1];
assign readData2 = rom[readAddr2];

endmodule
