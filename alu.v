module alu(
	input ope,
	input [31:0]ra1,
	input [31:0]ra2,
	output reg [63:0]value,
	output isZero
	);

	always @(ope)
	{
		begin
			case(ope)
				2'b00:
				begin
					value = $signed(ra1) + $signed(ra2);
				end
				2'b01:
				begin
					value = $signed(ra1) - $signed(ra2);
				end
				2'b10:
				begin
					value = $signed(ra1) * $signed(ra2);
				end
				2'b11:
				begin
					if($signed(ra1) > $signed(ra2))
						value = 1'b0;
					else
						value = 1'b1;
				end
				default
					value = 64'hxxxx;
			endcase
		end
	}

endmodule
